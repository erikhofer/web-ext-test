const packageJson = require('./package.json')
const shell = require('shelljs')
const { globSync } = require('glob')

shell.config.fatal = true

const version = packageJson.version
const { env } = process

// sign xpi
console.log('🚀 Signing XPI')
shell.exec(`npx web-ext sign --api-key ${env.AMO_API_KEY} --api-secret ${env.AMO_API_SECRET} --channel unlisted`)

const xpiFile = globSync(`web-ext-artifacts/*-${version}.xpi`)[0]

// upload xpi
console.log('🚀 Uploading XPI')
const baseUrl = `${env.CI_API_V4_URL}/projects/${env.CI_PROJECT_ID}`
const xpiUrl = `${baseUrl}/packages/generic/xpi/${version}/web-ext-test.xpi`
shell.exec(`curl --fail-with-body --header "JOB-TOKEN: ${env.CI_JOB_TOKEN}" --upload-file ${xpiFile} "${xpiUrl}"`)

// create update manifest
console.log('🚀 Creating update manifest')
const updateManifest = JSON.stringify({
  addons: {
    "web-ext-test@erikhofer.de": {
      updates: [{ version: version, update_link: xpiUrl }],
    },
  },
}, null, 2)
const updateManifestFile = "web-ext-artifacts/update.json"
shell.ShellString(updateManifest).to(updateManifestFile)

// upload update manifest
console.log('🚀 Uploading update manifest')
const updateManifestUrl = `${baseUrl}/packages/generic/update/latest/update.json`
// shell.exec(`curl --fail-with-body --request DELETE --header "JOB-TOKEN: ${env.CI_JOB_TOKEN}" "${updateManifestUrl}"`)
shell.exec(`curl --fail-with-body --header "JOB-TOKEN: ${env.CI_JOB_TOKEN}" --upload-file ${updateManifestFile} "${updateManifestUrl}"`)

// create release
console.log('🚀 Creating release')
const data = JSON.stringify({
  tag_name: env.CI_COMMIT_TAG,
  ref: env.CI_COMMIT_SHA,
  name: env.CI_COMMIT_TAG,
  description: 'Release ' + version,
  assets: {
    links: [
      {
        url: xpiUrl,
        name: 'web-ext-test.xpi',
        link_type: 'package'
      }
    ]
  }
}).replace(/"/g, '\\"')

shell.exec(`curl --fail-with-body --request POST --header "Content-Type: application/json" --header "JOB-TOKEN: ${env.CI_JOB_TOKEN}" --data "${data}" ${baseUrl}/releases`)
